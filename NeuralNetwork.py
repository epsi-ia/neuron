import tensorflow as tf
import random
import math

#AUFFREDOU Adrien
#DIDIER Julian


# ========== LINEAR ===============
def generate_linear_train_data(quantity):
	x_list = []
	y_list = []

	for i in range(0, quantity):
		x = random.randint(-100, 100)
		x_list.append(x)
		y_list.append(2.0*x+1.0)

	return x_list, y_list




def linear_train(loop_range):
	# linear model parameters
	W = tf.Variable([2.0], tf.float32)
	b = tf.Variable([1.0], tf.float32)

	# Model input and output
	x = tf.placeholder(tf.float32)
	linear_model = W * x + b
	y = tf.placeholder(tf.float32)

	#loss
	loss = tf.reduce_sum(tf.square(linear_model - y))

	# optimizer
	optimizer = tf.train.GradientDescentOptimizer(0.01)
	train = optimizer.minimize(loss)

	# train
	x_train, y_train = generate_linear_train_data(10)

	# training loop
	init = tf.global_variables_initializer()
	sess = tf.Session()
	sess.run(init) # reset values to wrong
	for i in range(loop_range):

	  sess.run(train, {x:x_train, y:y_train})

	# evaluate training accuracy
	curr_W, curr_b, curr_loss  = sess.run([W, b, loss], {x:x_train, y:y_train})
	print("Linear function = W: %s b: %s loss: %s"%(curr_W, curr_b, curr_loss))

		# ========== Exponential ===============
def generate_exponential_train_data(quantity):
	x_list = []
	y_list = []

	for i in range(0,quantity):
		x = random.randint(-20, 20)
		x_list.append(x)
		y_list.append(2.0**x +5)

	return x_list,y_list

def exponential_train(loop_range):
	# linear model parameters
	W = tf.Variable([2.0], tf.float32)
	b = tf.Variable([5.0], tf.float32)

	# Model input and output
	x = tf.placeholder(tf.float32)
	exponential_model = W**x +b
	y = tf.placeholder(tf.float32)

	#loss
	loss = tf.reduce_sum(tf.square(exponential_model - y))

	# optimizer
	optimizer = tf.train.GradientDescentOptimizer(0.01)
	train = optimizer.minimize(loss)

	# train
	x_train, y_train = generate_exponential_train_data(10)

	# training loop
	init = tf.global_variables_initializer()
	sess = tf.Session()
	sess.run(init) # reset values to wrong
	for i in range(loop_range):
	  sess.run(train, {x:x_train, y:y_train})
	 

	# evaluate training accuracy
	curr_W, curr_b, curr_loss  = sess.run([W, b, loss], {x:x_train, y:y_train})
	print("Exponential function = W: %s b: %s loss: %s"%(curr_W, curr_b, curr_loss))

			# ========== negExponential ===============
def generate_neg_exponential_train_data(quantity):
	x_list = []
	y_list = []

	for i in range(0,quantity):
		x = random.randint(0,10)
		x_list.append(x)
		y_list.append(2.**-x +4)

	return x_list,y_list

def negexponential_train(loop_range):
	# linear model parameters
	W = tf.Variable([2.0], tf.float32)
	b = tf.Variable([5.0], tf.float32)

	# Model input and output
	x = tf.placeholder(tf.float32)
	negexponential_model = W**-x +b
	y = tf.placeholder(tf.float32)

	#loss
	loss = tf.reduce_sum(tf.square(negexponential_model - y))

	# optimizer
	optimizer = tf.train.GradientDescentOptimizer(0.01)
	train = optimizer.minimize(loss)

	# train
	x_train, y_train = generate_neg_exponential_train_data(10)

	# training loop
	init = tf.global_variables_initializer()
	sess = tf.Session()
	sess.run(init) # reset values to wrong
	for i in range(loop_range):
	  sess.run(train, {x:x_train, y:y_train})
	 

	# evaluate training accuracy
	curr_W, curr_b, curr_loss  = sess.run([W, b, loss], {x:x_train, y:y_train})
	print("Negative exponential function = W: %s b: %s loss: %s"%(curr_W, curr_b, curr_loss))

		# ========== square ===============
def generate_square_train_data(quantity):
	x_list = []
	y_list = []

	for i in range(0,quantity):
		x = random.randint(-10, 10)
		x_list.append(x)
		y_list.append(2.0*(x**2) +5.0)

	return x_list,y_list

def square_train(loop_range):
	# linear model parameters
	W = tf.Variable([2.0], tf.float32)
	b = tf.Variable([5.0], tf.float32)

	# Model input and output
	x = tf.placeholder(tf.float32)
	square_model = W*x**2 +b
	y = tf.placeholder(tf.float32)

	#loss
	loss = tf.reduce_sum(tf.square(square_model - y))

	# optimizer
	optimizer = tf.train.GradientDescentOptimizer(0.01)
	train = optimizer.minimize(loss)

	# train
	x_train, y_train = generate_square_train_data(10)

	# training loop
	init = tf.global_variables_initializer()
	sess = tf.Session()
	sess.run(init) # reset values to wrong
	for i in range(loop_range):
	  sess.run(train, {x:x_train, y:y_train})
	 

	# evaluate training accuracy
	curr_W, curr_b, curr_loss  = sess.run([W, b, loss], {x:x_train, y:y_train})
	print("Square Function = W: %s b: %s loss: %s"%(curr_W, curr_b, curr_loss))


linear_train(10)
square_train(10)
exponential_train(100)

negexponential_train(100)