import random

import matplotlib.pyplot as plt
import numpy as np

#AUFFREDOU Adrien
#DIDIER Julian


datas = { }
for i in range (1,2):
    datas[i]= {"supervisor":{"a" : -3,"b":10}, "neuron": {}}
coords = []
absi=[]


#Function returning the result expected for a var x given. It can be simplified as follows :
def linear_supervisor(x):
        #return float -3*x+10
        return float(datas[key]["supervisor"]["a"]*x+datas[key]["supervisor"]["b"])
#Function processing a result, with :
#a = weight
#x = var randomized (-100-100)
#b = biais
def neuron(a,x,b):
    return float(a*x+b)

#Main Function, with neuron's weight and biais randomized (0,1)
def train(a=random.uniform(0, 1), b = random.randint(0,1)):
    count = 0
   

    while(count <10000):
        x = random.randint(-100,100)
        

        #DoWhile is not included in python, it prevents x to be equals to 0, otherwise the biais would be found instantly
        while(x == 0):

            x = random.randint(-1000, 1000)
            

        
        #-3*x + 10 
        supervisor_y = linear_supervisor(x)

        neuron_y = neuron(a,x,b)

        #we add to their respective tables the result x and y (neuron_y), they will be used in order to draw the leaning curve
        absi.append(x)
        coords.append(neuron_y)

        sub = supervisor_y - neuron_y
        
        a += sub/x
        count += 1

        if(sub == 0):
            #if the substraction is equals to 0, we compute sub again with x+1
            sub = linear_supervisor(x+1) - neuron(a,x+1,b)
            if(sub != 0):
                #if the sum is not equals to 0, it means that the weights and biais are differents
                a += sub
                sub = linear_supervisor(x) - neuron(a,x,b)
                b += sub

                print(str(count))

            
            return {"a": a, "b": b}
        elif(sub>0 and sub<a):
            b+=sub%x
            print("+"+str(sub%x))
        elif(sub<0 and sub>-a):
            b-=sub%x
            print("-"+str(sub%x))

for key,values in datas.items():
    
    print("--------Fonction : F"+ str(key)+ " -----------")
    datas[key]["neuron"]=train()

    t = np.linspace(-100,100,20) # 100 linearly spaced numbers

    def f1(t):
        return datas[key]["supervisor"]["a"]*t+datas[key]["supervisor"]["b"]

    def f2(t):
        return datas[key]["neuron"]["a"] * t +datas[key]["neuron"]["b"]


    plt.plot(t, f1(t), t, f2(t), 'ro',absi,coords,"g^")
    plt.show()


